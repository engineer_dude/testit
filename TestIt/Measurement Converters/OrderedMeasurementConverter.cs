﻿namespace TestIt
{
    using System.Collections.Generic;
    using System.Linq;

    /// <summary>
    /// A type of MeasurementConverter that creates a conversion Dictionary of (T,int)
    /// The order of the entries into the Dictionary occur as the measurements are encountered
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public class OrderedMeasurementConverter<T> : MeasurementConverter
    {
        private Dictionary<T, int> conversionDictionary = new Dictionary<T, int>();
        private int index = 0;

        /// <summary>
        /// Convert the measurement to an Int32Measurement
        /// </summary>
        /// <param name="measurement"></param>
        /// <returns></returns>
        public override Int32Measurement Convert(Measurement measurement)
        {
            Int32Measurement convertedMeasurement = new Int32Measurement();

            #region Minimum
            if (conversionDictionary.Keys.Contains((T)measurement.Minimum) == false)
            {
                conversionDictionary.Add((T)measurement.Minimum, index);
                index++;
            }

            convertedMeasurement.Minimum = conversionDictionary[(T)measurement.Minimum];
            #endregion

            #region Measured
            if (conversionDictionary.Keys.Contains((T)measurement.Measured) == false)
            {
                conversionDictionary.Add((T)measurement.Measured, index);
                index++;
            }

            convertedMeasurement.Measured = conversionDictionary[(T)measurement.Measured];
            #endregion

            #region Maximum
            if (conversionDictionary.Keys.Contains((T)measurement.Maximum) == false)
            {
                conversionDictionary.Add((T)measurement.Maximum, index);
                index++;
            }

            convertedMeasurement.Maximum = conversionDictionary[(T)measurement.Maximum]; 
            #endregion

            return convertedMeasurement;
        }

        /// <summary>
        /// Return the Key of the Dictionary, given the index
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public override object Original(int index)
        {
            return conversionDictionary.FirstOrDefault(keyvaluepair => keyvaluepair.Value == index).Key;
        }
    }
}
