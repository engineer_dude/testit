﻿namespace TestIt
{
    /// <summary>
    /// Abstract class that converts measurements into Int32Measurements.
    /// This is useful when the type is non-numeric such as System.Drawing.Color
    /// </summary>
    public abstract class MeasurementConverter
    {
        /// <summary>
        /// Convert a measurement into an Int32Meaasurement
        /// </summary>
        /// <param name="measurement"></param>
        /// <returns></returns>
        public abstract Int32Measurement Convert(Measurement measurement);

        /// <summary>
        /// Return the value associated with the given index parameter.
        /// This can be the Key of a Key-Value-Pair.
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public abstract object Original(int index);
    }
}
