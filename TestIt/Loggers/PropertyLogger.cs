namespace TestIt
{
    using System;
    using System.Reflection;

    /// <summary>
    /// An implementation of the abstract Logger type for logging to a property
    /// </summary>

    [Serializable]
    public class PropertyLogger : Logger
    {
        readonly object instance;
        PropertyInfo propertyInfo;

        /// <summary>
        /// Constructor
        /// </summary>
        public PropertyLogger(object instance, string propertyName)
        {
            this.instance = instance;

            propertyInfo = instance.GetType().GetProperty(propertyName);
            if (propertyInfo is null)
                throw new PropertyDoesNotExistExceptions($"Property '{propertyName}' does not exist");
        }

        /// <summary>
        /// Log the string to the property
        /// </summary>
        /// <param name="value"></param>
        public override void LogString(string value)
        {
            object oldValue = propertyInfo.GetValue(instance);

            string newValue = oldValue + value + Environment.NewLine;

            propertyInfo.SetValue(instance, newValue);
        }
    }
}