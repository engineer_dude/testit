﻿namespace TestIt
{
    using System;

    /// <summary>
    /// An abstract logging class that logs Measurements
    /// </summary>
    
    [Serializable]
    public abstract class Logger
    {
        /// <summary>
        /// The logged Measurement
        /// </summary>
        public Measurement LoggedMeasurement { get; set; }

        /// <summary>
        /// An abstract method for logging a string
        /// </summary>
        /// <param name="value"></param>
        public abstract void LogString(string value);

        /// <summary>
        /// A method for logging a Measurement
        /// </summary>
        /// <param name="measurement"></param>
        public void LogMeasurement(Measurement measurement)
        {
            LoggedMeasurement = measurement;

            string csv = measurement.ToCSV();

            LogString(csv);
        }
    }
}