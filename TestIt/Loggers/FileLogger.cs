﻿namespace TestIt
{
    using System;
    using System.IO;

    /// <summary>
    /// An implementation of the abstract Logger type for logging to a file system
    /// </summary>

    [Serializable]
    public class FileLogger : Logger
    {
        private readonly string filePath;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="filePath"></param>
        public FileLogger(string filePath)
        {
            this.filePath = filePath;
        }

        /// <summary>
        /// Log the string to the file
        /// </summary>
        /// <param name="value"></param>
        public override void LogString(string value)
        {
            File.AppendAllText(path: filePath, contents: value + Environment.NewLine);
        }
    }
}
