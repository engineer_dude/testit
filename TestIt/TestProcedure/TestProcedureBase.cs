﻿namespace TestIt
{
    using System;
    using System.Collections.Generic;
    using System.Threading;

    /// <summary>
    /// The abstract base class for test procedures
    /// </summary>
    [Serializable]
    public abstract class TestProcedure
    {
        /// <summary>
        /// The default constructor
        /// </summary>
        public TestProcedure()
        {

        }

        /// <summary>
        /// The abstract Name of the Test Procedure
        /// </summary>
        public abstract string Name { get; }

        /// <summary>
        /// The abstract Version of the Test Procedure
        /// </summary>
        public abstract Version Version { get; }

        /// <summary>
        /// The abstract Initialize method of the Test Procedure
        /// </summary>
        public abstract (bool success, string info) Initialize();

        /// <summary>
        /// The abstract Execute method of the Test Procedure
        /// </summary>
        public abstract (bool success, string info) Execute();

        /// <summary>
        /// The abstract CleanUp method of the Test Procedure
        /// </summary>
        public abstract (bool success, string info) CleanUp();

        /// <summary>
        /// Flag used for waiting for the user to hit the Ok selection
        /// </summary>
        public bool WaitForInstructionOk { get; set; } = true;

        /// <summary>
        /// Flag used for waiting for the user to hit the Cancel selection
        /// </summary>
        public bool WaitForInstructionCancel { get; set; } = true;

        /// <summary>
        /// Flag used for waiting for the user to hit the Yes selection
        /// </summary>
        public bool WaitForInstructionYes { get; set; } = true;

        /// <summary>
        /// Flag used for waiting for the user to hit the No selection
        /// </summary>
        public bool WaitForInstructionNo { get; set; } = true;

        /// <summary>
        /// Flag used for waiting for the user to enter after viewing the Instruction
        /// </summary>
        public bool WaitForInstructionString { get; set; } = true;

        /// <summary>
        /// An enumeration of the Instruction Response Sets
        /// </summary>
        public enum InstructionsResponseSet
        {
            /// <summary>
            /// Choose Ok and Cancel
            /// </summary>
            OKCancel,

            /// <summary>
            /// Choose Yes, No, and Cancel
            /// </summary>
            YesNoCancel,

            /// <summary>
            /// Choose a String
            /// </summary>
            String
        }

        /// <summary>
        /// An enumeration of the Instruction Responses
        /// </summary>
        public enum InstructionResponse
        {
            /// <summary>
            /// OK
            /// </summary>
            OK,

            /// <summary>
            /// Yes
            /// </summary>
            Yes,

            /// <summary>
            /// NO
            /// </summary>
            No,

            /// <summary>
            /// Cancel
            /// </summary>
            Cancel,

            /// <summary>
            /// String
            /// </summary>
            String
        }

        /// <summary>
        /// An enumeration of the Test States
        /// </summary>
        public enum TestState
        {
            /// <summary>
            /// Stopped
            /// </summary>
            Stopped,

            /// <summary>
            /// TestSelection
            /// </summary>
            TestSelection,

            /// <summary>
            /// Running
            /// </summary>
            Running
        }

        /// <summary>
        /// The property that can pass across AppDomains
        /// </summary>
        public AppDomainArgs args { get; set; }

        /// <summary>
        /// The static abortTesting
        /// </summary>
        protected static bool abortTesting = false;

        /// <summary>
        /// The virtual OnAbortTesting event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected virtual void OnAbortTesting(object sender, EventArgs e)
        {
            abortTesting = true;
        }

        /// <summary>
        /// The ShowInstructions method
        /// </summary>
        /// <param name="message"></param>
        /// <param name="imagePath"></param>
        /// <param name="instructionsResponseSet"></param>
        /// <returns></returns>
        public InstructionResponse ShowInstructions(string message, string imagePath, InstructionsResponseSet instructionsResponseSet)
        {
            if (string.IsNullOrEmpty(message))
                args.Instructions = "";
            else
                args.Instructions = message;

            if (string.IsNullOrEmpty(imagePath))
                args.ImagePath = "";
            else
                args.ImagePath = imagePath;

            args.InstructionResponseSetVisibility_YesNoCancel = instructionsResponseSet == InstructionsResponseSet.YesNoCancel;
            args.InstructionResponseSetVisibility_OKCancel = instructionsResponseSet == InstructionsResponseSet.OKCancel;
            args.InstructionResponseSetVisibility_String = instructionsResponseSet == InstructionsResponseSet.String;

            args.SetInstructionsToVisible = true;

            WaitForInstruction(instructionsResponseSet);

            args.SetInstructionsToVisible = false;
            args.ImagePath = "";

            args.InstructionResponseSetVisibility_YesNoCancel = false;
            args.InstructionResponseSetVisibility_OKCancel = false;
            args.InstructionResponseSetVisibility_String = false;

            return GetInstructionResponse(instructionsResponseSet);
        }

        private void WaitForInstruction(InstructionsResponseSet instructionsResponseSet)
        {
            if (instructionsResponseSet == InstructionsResponseSet.OKCancel)
            {
                WaitForInstructionOk = true;
                WaitForInstructionCancel = true;

                do { Thread.Sleep(1); } while (WaitForInstructionOk && WaitForInstructionCancel);
            }
            else if (instructionsResponseSet == InstructionsResponseSet.YesNoCancel)
            {
                WaitForInstructionYes = true;
                WaitForInstructionNo = true;
                WaitForInstructionCancel = true;

                do { Thread.Sleep(1); } while (WaitForInstructionYes && WaitForInstructionNo && WaitForInstructionCancel);
            }
            else if (instructionsResponseSet == InstructionsResponseSet.String)
            {
                WaitForInstructionString = true;

                do { Thread.Sleep(1); } while (WaitForInstructionString);
            }
            else
            {
                throw new Exception($"{instructionsResponseSet.ToString()} is not supported");
            }
        }

        private InstructionResponse GetInstructionResponse(InstructionsResponseSet instructionsResponseSet)
        {
            if (instructionsResponseSet == InstructionsResponseSet.OKCancel)
            {
                if (WaitForInstructionOk == false)
                    return InstructionResponse.OK;
                else
                    return InstructionResponse.Cancel;
            }
            else if (instructionsResponseSet == InstructionsResponseSet.YesNoCancel)
            {
                if (WaitForInstructionYes == false)
                    return InstructionResponse.Yes;
                else if (WaitForInstructionNo == false)
                    return InstructionResponse.No;
                else
                    return InstructionResponse.Cancel;
            }
            else
                throw new Exception($"{instructionsResponseSet.ToString()} is not supported");
        }

        /// <summary>
        /// Make a choice given the instructions and a list of choices
        /// </summary>
        /// <param name="instructions"></param>
        /// <param name="choices"></param>
        /// <returns></returns>
        public object MakeChoice(string instructions, List<object> choices)
        {
            if (string.IsNullOrEmpty(instructions))
                args.ChoiceInstructions = "";
            else
                args.ChoiceInstructions = instructions;

            args.Choices = choices;

            args.ChoicesSetVisibility = true;

            WaitForInstruction(InstructionsResponseSet.String);

            args.ChoicesSetVisibility = false;

            args.ChoiceInstructions = "";
            args.Choices = new List<object>();

            return args.Choice;
        }
    }
}
