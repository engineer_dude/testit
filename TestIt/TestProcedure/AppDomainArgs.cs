﻿namespace TestIt
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel;

    /// <summary>
    /// A class derived from MarshalByRefObject so that properties can cross the AppDomain
    /// INotifyPropertyChanged is added so that the event can cross the AppDomain
    /// </summary>
    public class AppDomainArgs : MarshalByRefObject, INotifyPropertyChanged
    {
        #region CurrentTestName
        private string currentTestName;

        /// <summary>
        /// CurrentTestName
        /// </summary>
        public string CurrentTestName
        {
            get { return currentTestName; }
            set
            {
                currentTestName = value;
                OnPropertyChanged("CurrentTestName");
            }
        }
        #endregion

        #region CurrentTestDetails
        private string currentTestDetails;
        
        /// <summary>
        /// CurrentTestDetail
        /// </summary>
        public string CurrentTestDetails
        {
            get { return currentTestDetails; }
            set
            {
                currentTestDetails = value;
                OnPropertyChanged("CurrentTestDetails");
            }
        }
        #endregion

        #region ImagePath
        private string imagePath;

        /// <summary>
        /// ImagePath
        /// </summary>
        public string ImagePath
        {
            get
            {
                return imagePath;
            }
            set
            {
                imagePath = value;
                OnPropertyChanged("ImagePath");
            }
        }
        #endregion

        #region Instructions
        private string instructions;

        /// <summary>
        /// Instructions
        /// </summary>
        public string Instructions
        {
            get { return instructions; }
            set
            {
                instructions = value;
                OnPropertyChanged("Instructions");
            }
        }
        #endregion

        #region SetInstructionsToVisible
        private bool setInstructionsToVisible;

        /// <summary>
        /// SetInstructionsToVisible: flag that can be used to indicate to the User Interface
        /// that the Instructions to should be shown
        /// </summary>
        public bool SetInstructionsToVisible
        {
            get { return setInstructionsToVisible; }
            set
            {
                setInstructionsToVisible = value;
                OnPropertyChanged("SetInstructionsToVisible");
            }
        }
        #endregion

        #region InstructionResponseSetVisibility_OKCancel
        private bool instructionResponseSetVisibility_OKCancel;

        /// <summary>
        /// Flag that can be used by the User Interface to show the OK and Cancel buttons
        /// </summary>
        public bool InstructionResponseSetVisibility_OKCancel
        {
            get { return instructionResponseSetVisibility_OKCancel; }
            set
            {
                instructionResponseSetVisibility_OKCancel = value;
                OnPropertyChanged("InstructionResponseSetVisibility_OKCancel");
            }
        }
        #endregion

        #region InstructionResponseSetVisibility_YesNoCancel
        private bool instructionResponseSetVisibilityYesNoCancel;

        /// <summary>
        /// Flag that can be used by the User Interface to show the Yes, No, and Cancel buttons
        /// </summary>
        public bool InstructionResponseSetVisibility_YesNoCancel
        {
            get { return instructionResponseSetVisibilityYesNoCancel; }
            set
            {
                instructionResponseSetVisibilityYesNoCancel = value;
                OnPropertyChanged("InstructionResponseSetVisibility_YesNoCancel");
            }
        }
        #endregion

        #region InstructionResponseSetVisibility_String
        private bool instructionResponseSetVisibility_String;

        /// <summary>
        /// A flag that can be used by the User Interface
        /// </summary>
        public bool InstructionResponseSetVisibility_String
        {
            get { return instructionResponseSetVisibility_String; }
            set
            {
                instructionResponseSetVisibility_String = value;
                OnPropertyChanged("InstructionResponseSetVisibility_String");
            }
        }
        #endregion

        #region SubTestNames
        private List<string> subTestNames = new List<string>();

        /// <summary>
        /// A list of Subtest Names.  Subtest methods must be decorated with the SubTestAttribute
        /// </summary>
        public List<string> SubTestNames
        {
            get { return subTestNames; }
            set
            {
                subTestNames = value;
                OnPropertyChanged("SubTestNames");
            }
        }
        #endregion

        #region INotifyPropertyChanged
        /// <summary>
        /// The PropertyChanged EventHandler
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged = delegate { };

        /// <summary>
        /// Method to invoke the PropertyChanged event
        /// </summary>
        /// <param name="propertyName"></param>
        protected void OnPropertyChanged(string propertyName)
        {
            PropertyChanged.Invoke(sender: this, e: new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        /// <summary>
        /// The EventHandler used for aborting the test
        /// </summary>
        public EventHandler AbortTestingEventHandler;

        private string choiceInstructions;

        /// <summary>
        /// A string that holds the users choice
        /// </summary>
        public string ChoiceInstructions
        {
            get { return choiceInstructions; }
            internal set
            {
                choiceInstructions = value;
                OnPropertyChanged("ChoiceInstructions");
            }
        }

        private List<object> choices = new List<object>();

        /// <summary>
        /// A list of choices
        /// </summary>
        public List<object> Choices
        {
            get { return choices; }
            internal set
            {
                choices = value;
                OnPropertyChanged("Choices");
            }
        }

        private bool choicesSetVisibility;

        /// <summary>
        /// A flag that can be used by the User Interface to set the visibility of the Choices
        /// </summary>
        public bool ChoicesSetVisibility
        {
            get { return choicesSetVisibility; }
            internal set
            {
                choicesSetVisibility = value;
                OnPropertyChanged("ChoicesSetVisibility");
            }
        }

        private object choice;

        /// <summary>
        /// The chosen Choice
        /// </summary>
        public object Choice
        {
            get { return choice; }
            set
            {
                choice = value;
                OnPropertyChanged("Choice");
            }
        }
    }
}
