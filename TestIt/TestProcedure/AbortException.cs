﻿namespace TestIt
{
    using System;
    using System.Runtime.Serialization;

    /// <summary>
    /// A class that handles exceptions that are thrown to indicate that the program should abort
    /// </summary>
    public class AbortException : Exception
    {
        /// <summary>
        /// Default AbortException
        /// </summary>
        public AbortException()
        {
        }

        /// <summary>
        /// AbortException that accepts a message
        /// </summary>
        /// <param name="message"></param>
        public AbortException(string message) : base(message)
        {
        }

        /// <summary>
        /// AbortException that accepts a message and an innerException
        /// </summary>
        /// <param name="message"></param>
        /// <param name="innerException"></param>
        public AbortException(string message, Exception innerException) : base(message, innerException)
        {
        }

        /// <summary>
        /// AbortException that accepts SerializationInfo and StreamingContext
        /// </summary>
        /// <param name="info"></param>
        /// <param name="context"></param>
        protected AbortException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
