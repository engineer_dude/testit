﻿namespace TestIt
{
    using System;

    /// <summary>
    /// An attribute that can be used to show that a method is a SubTest
    /// </summary>
    [AttributeUsage(validOn:AttributeTargets.All, AllowMultiple =true, Inherited =true)]
    public class SubTestAttribute : Attribute
    {
    }
}