﻿namespace TestIt
{
    using System;
    using System.Drawing;

    /// <summary>
    /// A type of MeasurementConverterFactory that produces an OrderedMeasurementConverterFactory
    /// </summary>
    public class OrderedMeasurementConverterFactory : MeasurementConverterFactory
    {
        /// <summary>
        /// Create a MeasurementConverter, given a measurement type
        /// </summary>
        /// <param name="measurementType"></param>
        /// <returns></returns>
        public override MeasurementConverter Create(Type measurementType)
        {
            if (measurementType == typeof(ColorMeasurement))
            {
                return new OrderedMeasurementConverter<Color>();
            }

            return null;
        }
    }
}
