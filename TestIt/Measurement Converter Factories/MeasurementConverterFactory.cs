﻿namespace TestIt
{
    using System;

    /// <summary>
    /// A factory that creates a MeasurementConverter, given a measurement type
    /// </summary>
    public abstract class MeasurementConverterFactory
    {
        /// <summary>
        /// Create the MeasurementConverter, given a measurement type
        /// </summary>
        /// <param name="measurementType"></param>
        /// <returns></returns>
        public abstract MeasurementConverter Create(Type measurementType);
    }
}
