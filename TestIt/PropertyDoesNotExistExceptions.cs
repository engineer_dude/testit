﻿using System;
using System.Runtime.Serialization;

namespace TestIt
{
    [Serializable]
    internal class PropertyDoesNotExistExceptions : Exception
    {
        public PropertyDoesNotExistExceptions()
        {
        }

        public PropertyDoesNotExistExceptions(string message) : base(message)
        {
        }

        public PropertyDoesNotExistExceptions(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PropertyDoesNotExistExceptions(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}