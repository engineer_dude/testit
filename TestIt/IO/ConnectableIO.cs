﻿namespace TestIt
{
    /// <summary>
    /// This is the interface used for communicating with IO devices that are connectable.
    /// </summary>
    public interface ConnectableIO
    {
        /// <summary>
        /// Address of the device
        /// </summary>
        string Address { get; set; }

        /// <summary>
        /// State of the connection
        /// </summary>
        bool IsConnected { get; set; }

        /// <summary>
        /// Connection method
        /// </summary>
        /// <param name="address"></param>
        /// <returns></returns>
        (bool success, string info) Connect(string address);

        /// <summary>
        /// Disconnection method
        /// </summary>
        /// <returns></returns>
        (bool success, string info) Disconnect();

        /// <summary>
        /// Write method
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        (bool success, string info) Write(string value);

        /// <summary>
        /// Read method
        /// </summary>
        /// <returns></returns>
        (bool success, string valueRead, string info) Read();
    }
}