# TestIt Test Development Framework
## A light-weight development Framework
---
### Features that have been implemented:
* (**Version 1.0**) Measurement Logging
* (**Version 1.0**) Measurement Retrieval

### Features that are being developed:

* (**Version 1.1**) A mock controller for testing
* (**Version 1.1**) A real VISA COM controller (e.g. the Agilent 82357B)
* (**Version 1.1**) Some bare-bones examples of test equipment projects that use the controllers

### Measurement Details
There are three types of measurements supported

* ColorMeasurement
* DoubleMeasurement
* Int32Measurement

All of the measurement types are based on the abstract Measurement class.  Each of the above measurement types override
the abstract properties and methods to correspond to the specific requirements for that type.

#### Measurement Properties
* Name
* Timestamp
* Minimum (**abstract**)
* Measured (**abstract**)
* Maximum (**abstract**)
* Unit
* Pass (**abstract**)
* MeasurementType (**abstract**)
* IsNumeric (**abstract**)

#### Measurement Methods

* ToCSV (**abstract**)
* FromCSV (**abstract**)
* GetMeasurementFromCSV (**static**)

### Logger Details
There are two types of loggers

* FileLogger
* PropertyLogger

Each of the loggers is based on the abstract Logger class

#### Logger Properties

* LoggedMeasurement

#### Logger Methods

* LogString (**abstract**)
* LogMeasurement

The logger types override the LogString method.

The PropertyLogger logs a string to a public string property.  This allows
in-memory operation.

The FileLogger logs strings to the file system.
