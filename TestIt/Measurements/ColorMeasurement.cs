﻿using System;

namespace TestIt
{
    using System.Collections.Generic;
    using System.Drawing;

    /// <summary>
    /// A measurement based on the abstract Measurement type and ConvertibleMeasurement (of Color) interface
    /// </summary>
    public class ColorMeasurement : Measurement, ConvertibleMeasurement<Color>
    {
        private Color minimumColor = Color.Empty;

        /// <summary>
        /// The minimum limit of the measurement
        /// </summary>
        public override object Minimum
        { get { return minimumColor; } set { minimumColor = (Color)value; } }

        private Color measuredColor = Color.Empty;

        /// <summary>
        /// The measured value
        /// </summary>
        public override object Measured
        { get { return measuredColor; } set { measuredColor = (Color)value; } }

        private Color maximumColor = Color.Empty;

        /// <summary>
        /// The higher limit of the measurement
        /// </summary>
        public override object Maximum
        { get { return maximumColor; } set { maximumColor = (Color)value; } }

        /// <summary>
        /// The read only status of the measurement passing
        /// </summary>
        public override bool Pass
        { get { return Measured.Equals(Minimum) || Measured.Equals(Maximum); } }

        /// <summary>
        /// The measurement type.  This is the type for Minimum, Measured, and Maximum.
        /// </summary>
        public override Type MeasurementType => typeof(Color);

        /// <summary>
        /// This value indicates if the MeasurementType is numeric.
        /// Examples of numeric are double and int.  Examples of non-numeric is System.Drawing.Color
        /// </summary>
        public override bool IsNumeric => false;

        /// <summary>
        /// This is a Dictionary that is used for converting non-numeric values to numeric values
        /// </summary>
        public Dictionary<Color, uint> ConversionDictionary { get; set; } = new Dictionary<Color, uint>();

        /// <summary>
        /// This method converts a comma-separated-value string to a list of measurements.
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        public override Measurement FromCSV(string csv)
        {
            string[] parts = csv.Split(',');

            ColorMeasurement measurement = new ColorMeasurement();

            measurement.Timestamp = Convert.ToDateTime(parts[1]);   // [1]
            measurement.Name = parts[2];                            // [2]
            measurement.Minimum = Color.FromName(parts[3]);         // [3]
            measurement.Measured = Color.FromName(parts[4]);        // [4]
            measurement.Maximum = Color.FromName(parts[5]);         // [5]
            measurement.Unit = parts[6];                            // [6]

            return measurement;
        }

        /// <summary>
        /// This converts a ColorMeasurement to a comma-separated-value string
        /// </summary>
        /// <returns></returns>
        public override string ToCSV()
        {
            string returnValue = "";
            returnValue += $"{GetType().ToString()},";
            returnValue += $"{Timestamp},";
            returnValue += $"{Name},";
            returnValue += $"{((Color)(Minimum)).Name},";
            returnValue += $"{((Color)(Measured)).Name},";
            returnValue += $"{((Color)(Maximum)).Name},";
            returnValue += $"{Unit},";
            returnValue += $"{Pass}";

            return returnValue;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public ColorMeasurement() : base()
        {
        }
    }
}
