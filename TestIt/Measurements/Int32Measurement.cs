﻿using System;
using System.Collections.Generic;

namespace TestIt
{
    /// <summary>
    /// A measurement based on the abstract Measurement type
    /// </summary>
    public class Int32Measurement : Measurement
    {
        private Int32 minimum = 0;

        /// <summary>
        /// The minimum limit of the measurement
        /// </summary>
        public override object Minimum
        { get { return minimum; } set { minimum = (Int32)value; } }

        private Int32 measured = 0;

        /// <summary>
        /// The measured value
        /// </summary>
        public override object Measured
        { get { return measured; } set { measured = (Int32)value; } }

        private Int32 maximum = 0;

        /// <summary>
        /// The higher limit of the measurement
        /// </summary>
        public override object Maximum
        { get { return maximum; } set { maximum = (Int32)value; } }

        /// <summary>
        /// The read only status of the measurement passing
        /// </summary>
        public override bool Pass
        {
            get
            {
                return (measured >= minimum && measured <= maximum) ||
                    (measured.Equals(minimum) && measured.Equals(maximum));
            }
        }

        /// <summary>
        /// The measurement type.  This is the type for Minimum, Measured, and Maximum.
        /// </summary>
        public override Type MeasurementType { get { return typeof(Int32); } }

        /// <summary>
        /// This value indicates if the MeasurementType is numeric.
        /// Examples of numeric are double and int.  Examples of non-numeric is System.Drawing.Color
        /// </summary>
        public override bool IsNumeric => true;

        /// <summary>
        /// This method converts a comma-separated-value string to a list of measurements.
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        public override Measurement FromCSV(string csv)
        {
            string[] parts = csv.Split(',');

            Int32Measurement measurement = new Int32Measurement();

            measurement.Timestamp = Convert.ToDateTime(parts[1]);   // [1]
            measurement.Name = parts[2];                            // [2]
            measurement.Minimum = Convert.ToInt32(parts[3]);        // [3]
            measurement.Measured = Convert.ToInt32(parts[4]);       // [4]
            measurement.Maximum = Convert.ToInt32(parts[5]);        // [5]
            measurement.Unit = parts[6];                            // [6]

            return measurement;
        }

        /// <summary>
        /// This converts an Int32Measurement to a comma-separated-value string
        /// </summary>
        /// <returns></returns>
        public override string ToCSV()
        {
            string returnValue = "";
            returnValue += $"{GetType().ToString()},";
            returnValue += $"{Timestamp},";
            returnValue += $"{Name},";
            returnValue += $"{Minimum},";
            returnValue += $"{Measured},";
            returnValue += $"{Maximum},";
            returnValue += $"{Unit},";
            returnValue += $"{Pass}";

            return returnValue;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public Int32Measurement() : base()
        {
        }

    }
}
