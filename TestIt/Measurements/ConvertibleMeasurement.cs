﻿namespace TestIt
{
    using System.Collections.Generic;

    /// <summary>
    /// This interface can be used for types derived from Measurement that need a way to 
    /// convert from non-numeric to numeric measurement types
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public interface ConvertibleMeasurement<T>
    {
        /// <summary>
        /// The Dictionary that holds the conversion values between non-numeric measurement types to numeric measurement types
        /// </summary>
        Dictionary<T, uint> ConversionDictionary { get; set; }
    }
}
