﻿using System;
using System.Collections.Generic;

namespace TestIt
{
    /// <summary>
    /// Abstract Measurement type (used by DoubleMeasurement, Int32Measurement, and ColorMeasurement
    /// </summary>
    public abstract class Measurement
    {
        /// <summary>
        /// Name of the measurement
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The timestampe of the measurement
        /// </summary>
        public DateTime Timestamp { get; set; }

        /// <summary>
        /// The minimum limit of the measurement
        /// </summary>
        public abstract object Minimum { get; set; }

        /// <summary>
        /// The measured value
        /// </summary>
        public abstract object Measured { get; set; }

        /// <summary>
        /// The higher limit of the measurement
        /// </summary>
        public abstract object Maximum { get; set; }

        /// <summary>
        /// The unit of the measurement
        /// </summary>
        public string Unit { get; set; }

        /// <summary>
        /// The read only status of the measurement passing
        /// </summary>
        public abstract bool Pass { get; }

        /// <summary>
        /// The measurement type.  This is the type for Minimum, Measured, and Maximum.
        /// </summary>
        public abstract Type MeasurementType { get; }

        /// <summary>
        /// This value indicates if the MeasurementType is numeric.
        /// Examples of numeric are double and int.  Examples of non-numeric is System.Drawing.Color
        /// </summary>
        public abstract bool IsNumeric { get; }

        /// <summary>
        /// This converts a Measurement to a comma-separated-value string
        /// </summary>
        /// <returns></returns>
        public abstract string ToCSV();

        /// <summary>
        /// This method converts a comma-separated-value string to a list of measurements.
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        public abstract Measurement FromCSV(string csv);

        /// <summary>
        /// Constructor
        /// </summary>
        public Measurement()
        {
            Name = "";
            Timestamp = new DateTime();
            Unit = "";
        }

        /// <summary>
        /// A method that takes a comma-separated-values string and produces a list of Measurement
        /// </summary>
        /// <param name="csv"></param>
        /// <returns></returns>
        public static List<Measurement> GetMeasurementsFromCSV(string csv)
        {
            List<Measurement> measurements = new List<Measurement>();

            string[] lines = csv.Split(separator: new string[] { Environment.NewLine }, options: StringSplitOptions.RemoveEmptyEntries);

            foreach (string line in lines)
            {
                Measurement measurement = MeasurementFactory.Create(line);

                measurements.Add(measurement);
            }

            return measurements;
        }
    }
}