﻿namespace TestIt
{
    using System;
    using System.Drawing;

    internal class MeasurementFactory
    {
        internal static Measurement Create(string measurementString)
        {
            string[] parts = measurementString.Split(',');

            Measurement measurement = null;

            if (parts.Length == 0)
            {
                throw new IndexOutOfRangeException("The measurement type was not found");
            }

            if (parts[0] == "TestIt.Int32Measurement")
            {
                if (parts.Length != 8)
                {
                    throw new ArgumentException($"Expected 8 parts, but found {parts.Length}");
                }

                measurement = new Int32Measurement();

                #region Minimum
                try
                {
                    measurement.Minimum = Convert.ToInt32(parts[3]);       // [3]
                }
                catch
                {
                    throw new ArgumentException($"Error converting Minimum");
                }
                #endregion

                #region Measured
                try
                {
                    measurement.Measured = Convert.ToInt32(parts[4]);      // [4]
                }
                catch
                {
                    throw new ArgumentException($"Error converting Measured");
                }
                #endregion

                #region Maximum
                try
                {
                    measurement.Maximum = Convert.ToInt32(parts[5]);       // [5]
                }
                catch
                {
                    throw new ArgumentException($"Error converting Maximum");
                }
                #endregion

            }
            else if (parts[0] == "TestIt.DoubleMeasurement")
            {
                if (parts.Length != 8)
                {
                    throw new ArgumentException($"Expected 8 parts, but found {parts.Length}");
                }

                measurement = new DoubleMeasurement();

                #region Minimum
                try
                {
                    measurement.Minimum = Convert.ToDouble(parts[3]);       // [3]
                }
                catch
                {
                    throw new ArgumentException($"Error converting Minimum");
                }
                #endregion

                #region Measured
                try
                {
                    measurement.Measured = Convert.ToDouble(parts[4]);      // [4]
                }
                catch
                {
                    throw new ArgumentException($"Error converting Measured");
                }
                #endregion

                #region Maximum
                try
                {
                    measurement.Maximum = Convert.ToDouble(parts[5]);       // [5]
                }
                catch
                {
                    throw new ArgumentException($"Error converting Maximum");
                } 
                #endregion

            }
            else if (parts[0] == "TestIt.ColorMeasurement")
            {
                if (parts.Length != 8)
                {
                    throw new ArgumentException($"Expected 8 parts, but found {parts.Length}");
                }

                measurement = new ColorMeasurement();

                measurement.Minimum = Color.FromName(parts[3]);
                CheckColorValidity(measurement.Minimum, "Minimum");

                measurement.Measured = Color.FromName(parts[4]);
                CheckColorValidity(measurement.Measured, "Measured");

                measurement.Maximum = Color.FromName(parts[5]);
                CheckColorValidity(measurement.Maximum, "Maximum");
            }
            else
            {
                throw new ArgumentException($"The factory does not create the type '{parts[0]}'");
            }

            try
            {
                measurement.Timestamp = Convert.ToDateTime(parts[1]);   // [1]
            }
            catch (FormatException ex)
            {
                throw new ArgumentException(message: ex.Message, innerException: ex);
            }

            measurement.Name = parts[2];                            // [2]
            measurement.Unit = parts[6];                            // [6]

            // parts[7] contains the 'Pass' value.  This value is only checked for validity
            // If invalid, an exception will be thrown.  The value is not assigned, as it only has a getter

            try
            {
                bool pass = Convert.ToBoolean(parts[7]);
            }
            catch (FormatException ex)
            {
                throw new ArgumentException(message: ex.Message, innerException: ex);
            }

            return measurement;
        }

        private static void CheckColorValidity(object value, string name)
        {
            if (((Color)value).A == 0 && ((Color)value).R == 0 &&
                ((Color)value).G == 0 && ((Color)value).B == 0 && ((Color)value).Name != "0")
            {
                throw new ArgumentException($"Error converting {name}");
            }
        }
    }
}