﻿namespace A34970A
{
    using TestIt;
    using System;

    public class A34970A
    {
        ConnectableIO controller;
        readonly string visaAddress;

        public A34970A(ConnectableIO controller, string address)
        {
            this.controller = controller;
            visaAddress = $"{controller.Address}::{address}::INSTR";
        }

        public (bool success, string info) Reset()
        {
            return WriteTransaction(writeString: "*RST");
        }

        public (bool success, string identity, string info) Identify()
        {
            return QueryTransaction_String(queryString: "*IDN?");
        }

        public (bool success, double voltage, string info) MeasureVDC(double range, double resolution, ushort channel)
        {
            return QueryTransaction_Double(queryString: $"MEAS:VOLT:DC? {range},{resolution}, (@{channel})");
        }

        #region Transactions
        private (bool success, string info) WriteTransaction(string writeString)
        {
            (bool success, string info) connectResult = controller.Connect(visaAddress);

            (bool success, string info) writeResult = controller.Write(writeString);

            (bool success, string info) disconnectResult = disconnectResult = controller.Disconnect();

            bool success = connectResult.success && writeResult.success && disconnectResult.success;
            string info = connectResult.info + "\n" + writeResult.info + "\n" + disconnectResult.info;

            return (success, info);
        }

        private (bool success, double value, string info) QueryTransaction_Double(string queryString)
        {
            (bool success, string info) connectResult = controller.Connect(visaAddress);

            (bool success, string info) writeResult = controller.Write(queryString);

            (bool success, string valueRead, string info) readResult = controller.Read();

            bool successfulConversion = false;
            double value = double.NaN;

            string[] parts = readResult.valueRead.Split(separator: new string[] { " " }, options: StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 1)
            {
                successfulConversion = double.TryParse(parts[0].Trim(new char[] { '\n' }), out value);
            }

            string successfulConversionInfo = successfulConversion ? "Successfully converted value" : "Unsuccessfully converted value";

            (bool success, string info) disconnectResult = controller.Disconnect();

            bool success = connectResult.success && writeResult.success && readResult.success && successfulConversion && disconnectResult.success;
            string info = connectResult.info + "\n" + writeResult.info + "\n" + readResult.info + "\n" + successfulConversionInfo + "\n" + disconnectResult.info;

            return (success: success, value: value, info: info);
        }

        private (bool success, string value, string info) QueryTransaction_String(string queryString)
        {
            (bool success, string info) connectResult = controller.Connect(visaAddress);

            (bool success, string info) writeResult = controller.Write(queryString);

            (bool success, string valueRead, string info) readResult = controller.Read();

            (bool success, string info) disconnectResult = controller.Disconnect();

            bool success = connectResult.success && writeResult.success && readResult.success && disconnectResult.success;
            string info = connectResult.info + "\n" + writeResult.info + "\n" + readResult.info + "\n" + disconnectResult.info;

            return (success: success, value: readResult.valueRead, info: info);
        }
        #endregion
    }
}


