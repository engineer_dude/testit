﻿namespace Agilent_82357B_USB_GPIB_Controller
{
    using System;
    using TestIt;

    public class Agilent_82357B_Controller : ConnectableIO
    {
        private Ivi.Visa.Interop.IResourceManager3 rm;
        private Ivi.Visa.Interop.IFormattedIO488 ioObj;

        #region ConnectableIO Properties
        public string Address { get; set; } = "";

        public bool IsConnected { get; set; } = false;
        #endregion

        #region Constructor
        public Agilent_82357B_Controller(string controllerAddress)
        {
            Address = controllerAddress;
        }
        #endregion

        #region Connectable Methods
        public (bool success, string info) Connect(string visaAddress)
        {
            Address = visaAddress;
            if (IsConnected)
                return (success: false, info: "Error - Already connected");

            try
            {
                rm = new Ivi.Visa.Interop.ResourceManager();
                ioObj = new Ivi.Visa.Interop.FormattedIO488();

                ioObj.IO = (Ivi.Visa.Interop.IMessage)rm.Open(visaAddress,
                    Ivi.Visa.Interop.AccessMode.NO_LOCK, 0, "");

                IsConnected = true;

                return (success: true, info: $"Successfully connected");
            }
            catch (Exception)
            {
                IsConnected = false;

                return (success: false, info: $"Connection was not successful");
            }
        }

        public (bool success, string info) Disconnect()
        {
            try { ioObj.IO.Close(); }
            catch { }
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(ioObj);
            }
            catch { }
            try
            {
                System.Runtime.InteropServices.Marshal.ReleaseComObject(rm);
            }
            catch { }

            IsConnected = false;

            return (success: true, info: $"Successfully disconnected from address {Address}");
        }

        public (bool success, string info) Write(string value)
        {
            if (IsConnected == false)
            {
                return (success: false, info: "Error - Connection is closed");
            }

            try
            {
                ioObj.WriteString(value, true);

                return (success: true, $"Successfully wrote {value} to address {Address}");
            }
            catch (Exception ex)
            {
                return (success: false, $"Unsuccessfully wrote {value} to address {Address}\n" + ex.Message);
            }
        }

        public (bool success, string valueRead, string info) Read()
        {
            string valueRead = "";

            if (IsConnected == false)
            {
                return (success: false, valueRead: valueRead, info: "Error - Connection is closed");
            }

            try
            {
                valueRead = ioObj.ReadString();

                return (success: true, valueRead: valueRead, info: $"Successfully read '{valueRead}' from address {Address}");
            }
            catch (Exception ex)
            {
                return (success: false, valueRead: valueRead, info: $"Unsuccessfully read from address {Address}\n" + ex.Message);
            }
        } 
        #endregion
    }
}
