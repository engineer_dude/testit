﻿namespace SorensenXHR
{
    using System;
    using TestIt;

    public class SorensenXHR
    {
        private ConnectableIO controller;
        readonly string visaAddress;

        public SorensenXHR(ConnectableIO controller, string address)
        {
            this.controller = controller;
            visaAddress = $"{controller.Address}::{address}::INSTR";
        }

        public (bool success, string identity, string info) Identify()
        {
            return QueryTransaction_String(queryString: "ID?");
        }

        public (bool success, string info) SetVoltage(double value)
        {
            return WriteTransaction($"VSET {value}");
        }

        public (bool success, string info) SetCurrent(double value)
        {
            return WriteTransaction($"ISET {value}");
        }

        public (bool success, double voltage, string info) GetSetVoltage()
        {
            return QueryTransaction_Double(queryString: "VSET?");
        }

        public (bool success, double current, string info) GetSetCurrent()
        {
            return QueryTransaction_Double(queryString: "ISET?");
        }

        #region Transactions
        private (bool success, string info) WriteTransaction(string writeString)
        {
            (bool success, string info) connectResult = controller.Connect(visaAddress);

            (bool success, string info) writeResult = controller.Write(writeString);

            (bool success, string info) disconnectResult = disconnectResult = controller.Disconnect();

            bool success = connectResult.success && writeResult.success && disconnectResult.success;
            string info = connectResult.info + "\n" + writeResult.info + "\n" + disconnectResult.info;

            return (success, info);
        }

        private (bool success, double value, string info) QueryTransaction_Double(string queryString)
        {
            (bool success, string info) connectResult = controller.Connect(visaAddress);

            (bool success, string info) writeResult = controller.Write(queryString);

            (bool success, string valueRead, string info) readResult = controller.Read();

            bool successfulConversion = false;
            double value = double.NaN;

            string[] parts = readResult.valueRead.Split(separator: new string[] { " " }, options: StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length == 3)
            {
                successfulConversion = double.TryParse(parts[1], out value);
            }

            string successfulConversionInfo = successfulConversion ? "Successfully converted value" : "Unsuccessfully converted value";

            (bool success, string info) disconnectResult = controller.Disconnect();

            bool success = connectResult.success && writeResult.success && readResult.success && successfulConversion && disconnectResult.success;
            string info = connectResult.info + "\n" + writeResult.info + "\n" + readResult.info + "\n" + successfulConversionInfo + "\n" + disconnectResult.info;

            return (success: success, value: value, info: info);
        }

        private (bool success, string value, string info) QueryTransaction_String(string queryString)
        {
            (bool success, string info) connectResult = controller.Connect(visaAddress);

            (bool success, string info) writeResult = controller.Write(queryString);

            (bool success, string valueRead, string info) readResult = controller.Read();

            (bool success, string info) disconnectResult = controller.Disconnect();

            bool success = connectResult.success && writeResult.success && readResult.success && disconnectResult.success;
            string info = connectResult.info + "\n" + writeResult.info + "\n" + readResult.info + "\n" + disconnectResult.info;

            return (success: success, value: readResult.valueRead, info: info);
        } 
        #endregion
    }
}
