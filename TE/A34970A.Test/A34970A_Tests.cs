﻿namespace A34970A.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MockGPIB_Controller;

    [TestClass]
    public class A34970A_Tests
    {
        MockGPIB_Controller controller;
        A34970A dataAcq;

        [TestInitialize]
        public void Initialize()
        {
            controller = new MockGPIB_Controller(controllerAddress: "GPIB0");
            dataAcq = new A34970A(controller: controller, address: "11");
        }

        [TestMethod]
        public void Reset()
        {
            (bool success, string info) = dataAcq.Reset();

            controller.WriteSuccess = true;

            Assert.IsTrue(success, message: info);
        }

        [TestMethod]
        public void Identify_ValidResponse_ExpectSuccess()
        {
            controller.ValueRead = "34970A";

            (bool success, string identity, string info) = dataAcq.Identify();

            Assert.IsTrue(success, info);
            Assert.AreEqual(expected: "34970A", actual: identity, message: info);
        }

        [TestMethod]
        public void Identify_InvalidResponse_ExpectFail()
        {
            controller.ValueRead = "";

            (bool success, string identity, string info) = dataAcq.Identify();

            Assert.IsTrue(success, info);
            Assert.AreNotEqual(notExpected: "34970A", actual: identity, message: info);
        }

    }
}
