﻿namespace A34970A.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using TestIt;
    using Agilent_82357B_USB_GPIB_Controller;

    [TestClass]
    public class A34970A_Integration_Tests
    {
        ConnectableIO controller;
        A34970A dataAcq;

        [TestInitialize]
        public void Initialize()
        {
            controller = new Agilent_82357B_Controller(controllerAddress: "GPIB0");
            dataAcq = new A34970A(controller, address: "11");
        }

        [TestMethod]
        public void Given_controller_not_connected_to_computer_When_Identify_ThenFail()
        {
            var result = dataAcq.Identify();

            Assert.AreEqual(expected: "", actual: result.identity, message: "Identity");
            Assert.AreEqual(expected: "Connection was not successful\n" + 
                "Error - Connection is closed\n" +
                "Error - Connection is closed\n" +
                "Successfully disconnected from address GPIB0::11::INSTR", actual: result.info, message: "Info");
            Assert.IsFalse(result.success, "Success");
        }

        [TestMethod]
        public void Given_controller_not_connected_to_instrument_When_Identify_ThenFail()
        {
            var result = dataAcq.Identify();

            Assert.AreEqual(expected: "", actual: result.identity, message: "Identity");
            Assert.AreEqual(expected: "Connection was not successful\n" +
                "Error - Connection is closed\n" +
                "Error - Connection is closed\n" +
                "Successfully disconnected from address GPIB0::11::INSTR", actual: result.info, message: "Info");
            Assert.IsFalse(result.success, "Success");
        }

        [TestMethod]
        public void Given_controller_address_does_not_match_actual_controller_address_When_Identify_ThenFail()
        {
            controller = new Agilent_82357B_Controller(controllerAddress: "GPIB1");
            dataAcq = new A34970A(controller, address: "11");

            var result = dataAcq.Identify();

            Assert.AreEqual(expected: "", actual: result.identity, message: "Identity");
            Assert.AreEqual(expected: "Connection was not successful\n" +
                "Error - Connection is closed\n" +
                "Error - Connection is closed\n" +
                "Successfully disconnected from address GPIB1::11::INSTR", actual: result.info, message: "Info");
            Assert.IsFalse(result.success, "Success");
        }

        [TestMethod]
        public void Given_instrument_address_does_not_match_actual_instrument_address_When_Identify_ThenFail()
        {
            dataAcq = new A34970A(controller, address: "12");

            var result = dataAcq.Identify();

            Assert.AreEqual(expected: "Connection was not successful\n" +
                "Error - Connection is closed\n" +
                "Error - Connection is closed\n" +
                "Successfully disconnected from address GPIB0::12::INSTR", actual: result.info, message: "Info");
            Assert.IsFalse(result.success, "Success");
        }

        [TestMethod]
        public void Given_two_instruments_have_address_11_WhenIdentify_ThenFail()
        {
            var result = dataAcq.Identify();

            Assert.AreNotEqual(notExpected: "HEWLETT-PACKARD,34970A,0,13-2-2\n", actual: result.identity, message: "Identity");
            Assert.AreNotEqual(notExpected: "Successfully connected\n" +
                "Successfully wrote *IDN? to address GPIB0::11::INSTR\n" +
                "Successfully read 'HEWLETT-PACKARD,34970A,0,13-2-2\n" +
                "' from address GPIB0::11::INSTR\n" +
                "Successfully disconnected from address GPIB0::11::INSTR", actual: result.info, message: "Info");
            Assert.IsTrue(result.success, "Success");
        }

        [TestMethod]
        public void Given_controller_driver_not_installed_WhenIdentify_ThenFail()
        {
            var result = dataAcq.Identify();

            Assert.AreEqual(expected: "", actual: result.identity, message: "Identity");
            Assert.AreEqual(expected: "Connection was not successful\n" +
                "Error - Connection is closed\n" +
                "Error - Connection is closed\n" +
                "Successfully disconnected from address GPIB0::11::INSTR", actual: result.info, message: "Info");
            Assert.IsFalse(result.success, "Success");
        }
    }
}
