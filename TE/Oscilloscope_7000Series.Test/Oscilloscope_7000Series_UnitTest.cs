﻿namespace Oscilloscope_7000Series.Test
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MockGPIB_Controller;

    [TestClass]
    public class Oscilloscope_7000Series_UnitTest
    {
        MockGPIB_Controller controller;
        Oscilloscope_7000Series scope;

        [TestInitialize]
        public void Initialize()
        {
            controller = new MockGPIB_Controller(controllerAddress: "USB0");

            scope = new Oscilloscope_7000Series(
                controller: controller,
                address: "Oscilloscope1");
        }

        [TestMethod]
        public void Reset()
        {
            (bool success, string info) = scope.Reset();

            controller.WriteSuccess = true;

            Assert.IsTrue(success, message: info);
        }

        [TestMethod]
        public void Identify_ValidResponse_ExpectSuccess()
        {
            controller.ValueRead = "DSO7034B";

            (bool success, string identity, string info) = scope.Identify();

            Assert.IsTrue(success, info);
            Assert.AreEqual(expected: "DSO7034B", actual: identity, message: info);
        }

        [TestMethod]
        public void Identify_InvalidResponse_ExpectFail()
        {
            controller.ValueRead = "";

            (bool success, string identity, string info) = scope.Identify();

            Assert.IsTrue(success, info);
            Assert.AreNotEqual(notExpected: "DSO7034B", actual: identity, message: info);
        }
    }
}
