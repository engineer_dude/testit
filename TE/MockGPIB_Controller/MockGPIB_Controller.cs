﻿namespace MockGPIB_Controller
{
    using TestIt;

    public class MockGPIB_Controller : ConnectableIO, MockGPIB_ControllerFunctionality
    {
        #region MockGPIB_ControllerFunctionality
        public bool DisconnectSuccess { get; set; } = true;
        public bool WriteSuccess { get; set; } = true;
        public bool ReadSuccess { get; set; } = true;
        public string ValueRead { get; set; } = "";
        public string ValueWritten { get; set; } = "";
        public string Info { get; set; } = ""; 
        #endregion

        #region Constructor
        public MockGPIB_Controller(string controllerAddress)
        {
            Address = controllerAddress;
        }
        #endregion

        #region ConnectableIO
        public string Address { get; set; } = "0";

        public bool IsConnected { get; set; } = false;

        public (bool success, string info) Connect(string address)
        {
            if (IsConnected)
                return (success: false, info: "Error - Already connected");

            IsConnected = true;

            return (success: true, info: "Successfully connected");
        }

        public (bool success, string info) Disconnect()
        {
            IsConnected = false;

            return (success: DisconnectSuccess, "Successfully disconnected");
        }

        public (bool success, string info) Write(string value)
        {
            ValueWritten = "";

            if (IsConnected == false)
                return (success: false, info: "Error - Connection is closed");

            if (WriteSuccess == false)
                return (success: false, info: "Error -  Write error");

            ValueWritten = value;

            return (success: true, info: $"Success - Value written is {ValueWritten}");
        }

        public (bool success, string valueRead, string info) Read()
        {
            if (IsConnected == false)
                return (success: false, valueRead: "", info: "Error - Connection is closed");

            if (ReadSuccess == false)
                return (success: false, valueRead: "", info: "Error - Read error");

            return (success: true, valueRead: ValueRead, info: "Success");
        } 
        #endregion
    }
}
