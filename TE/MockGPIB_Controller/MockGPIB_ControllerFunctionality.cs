﻿namespace MockGPIB_Controller
{
    public interface MockGPIB_ControllerFunctionality
    {
        bool DisconnectSuccess { get; set; }
        bool WriteSuccess { get; set; }
        bool ReadSuccess { get; set; }
        string ValueRead { get; set; }
        string ValueWritten { get; set; }
        string Info { get; set; }
    }
}