﻿namespace LoggingAndChartingDemo
{
    using System;
    using System.Collections.Generic;
    using System.IO;
    using System.Linq;
    using System.Windows;
    using TestIt;
    using System.Drawing;

    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Constructor
        public MainWindow()
        {
            InitializeComponent();
        }
        #endregion

        #region Private fields
        private Logger logger = null;

        private List<Measurement> measurements = new List<Measurement>();
        #endregion

        #region Button Click EventHandlers
        private void ButtonLogData_Click(object sender, RoutedEventArgs e)
        {
            File.Delete(@".\LogFile.csv");
            logger = new FileLogger(@".\LogFile.csv");

            Random random = new Random();

            for (int i = 0; i < 10; i++)
            {
                double outputVoltage = 5.0 + random.NextDouble() * 0.1 - 0.05;

                Measurement measurement = new DoubleMeasurement()
                { Name = "VIN", Timestamp = new DateTime(), Minimum = 4.9, Measured = outputVoltage, Maximum = 5.1, Unit = "V" };

                logger.LogMeasurement(measurement);

                Int32 registerValue = random.NextDouble() < 0.5 ? 8 : 9;

                measurement = new Int32Measurement()
                { Name = "Status Register", Timestamp = new DateTime(), Minimum = 8, Measured = registerValue, Maximum = 8, Unit = "Byte" };

                logger.LogMeasurement(measurement);

                double randomResult = random.NextDouble();

                Color ledColor;
                if (randomResult < 0.3)
                    ledColor = Color.Green;
                else if (randomResult < 0.7)
                    ledColor = Color.Red;
                else
                    ledColor = Color.Blue;

                measurement = new ColorMeasurement()
                { Name = "Power Good LED", Timestamp = new DateTime(), Minimum = Color.Red, Measured = ledColor, Maximum = Color.Red, Unit = "Color" };

                logger.LogMeasurement(measurement);
            }
        }

        private void ButtonShowData_Click(object sender, RoutedEventArgs e)
        {
            List<MinMeasMaxLineChart> charts = new List<MinMeasMaxLineChart> { chart0, chart1, chart2, chart3 };

            ClearChartData(charts);

            string content = File.ReadAllText(@".\LogFile.csv");
            measurements = Measurement.GetMeasurementsFromCSV(content);

            string chartName = "<No Chart Name>";

            var measurementGroups = measurements.GroupBy(meas => meas.Name).ToList();

            int groupIndex = 0;

            foreach (MinMeasMaxLineChart chart in charts)
            {
                if (groupIndex >= measurementGroups.Count)
                    break;

                List<Measurement> measurementGroup = measurementGroups[groupIndex].ToList();

                bool allMeasurementTypesAreNumeric = measurementGroup.All(meas => meas.IsNumeric == true);
                bool allMeasurementTypesAreNonNumeric = measurementGroup.All(meas => meas.IsNumeric == false);

                if (measurementGroup.Count > 0)
                {
                    bool allMeasurementTypeHaveSameMeasurementType = measurementGroup.All(meas => meas.MeasurementType == measurementGroup[0].MeasurementType);

                    if (allMeasurementTypeHaveSameMeasurementType == false)
                    {
                        throw new Exception("The group of measurements have a mix of measurement types");
                    }
                }

                if (allMeasurementTypesAreNumeric == false && allMeasurementTypesAreNonNumeric == false)
                {
                    throw new Exception("The group of measurements have a mix of numeric and non-numerica measurement types");
                }

                if (measurementGroup.Count > 0)
                {
                    Type measurementType = measurementGroup[0].GetType();

                    chartName = measurementGroup[0].Name;

                    MeasurementConverterFactory measurementConverterFactory = null;
                    MeasurementConverter measurementConverter = null;

                    if (allMeasurementTypesAreNonNumeric)
                    {
                        measurementConverterFactory = new OrderedMeasurementConverterFactory();
                        measurementConverter = measurementConverterFactory.Create(measurementType);
                    }

                    for (int i = 0; i < measurementGroup.Count; i++)
                    {
                        if (allMeasurementTypesAreNonNumeric)
                        {
                            Int32Measurement convertedMeasurement = measurementConverter.Convert(measurementGroup[i]);

                            measurementGroup[i] = convertedMeasurement;
                        }

                        AddValuesToChart(chart: chart, index: i, measurement: measurementGroup[i], chartName: chartName);

                        UpdateMinimumAndMaximumYValues(chart);
                    }
                }

                groupIndex++;
            }
        }
        #endregion

        #region Private Helper Methods
        private void ClearChartData(List<MinMeasMaxLineChart> charts)
        {
            foreach (MinMeasMaxLineChart chart in charts)
            {
                chart.ChartValuesMinimum.Clear();
                chart.ChartValuesMeasured.Clear();
                chart.ChartValuesMaximum.Clear();
            }
        }

        private void AddValuesToChart(MinMeasMaxLineChart chart, int index, Measurement measurement, string chartName)
        {
            chart.ChartName = chartName;

            MeasureModel measureModelMinimum = new MeasureModel { Index = index, Value = Convert.ToDouble(measurement.Minimum) };
            chart.ChartValuesMinimum.Add(measureModelMinimum);

            MeasureModel measureModelMeasured = new MeasureModel { Index = index, Value = Convert.ToDouble(measurement.Measured) };
            chart.ChartValuesMeasured.Add(measureModelMeasured);

            MeasureModel measureModelMaximum = new MeasureModel { Index = index, Value = Convert.ToDouble(measurement.Maximum) };
            chart.ChartValuesMaximum.Add(measureModelMaximum);
        }

        private void UpdateMinimumAndMaximumYValues(MinMeasMaxLineChart chart)
        {
            // Get the minimum value of each of the three series
            double minMin = chart.ChartValuesMinimum.Min(x => x.Value);
            double minMeas = chart.ChartValuesMeasured.Min(x => x.Value);
            double minMax = chart.ChartValuesMaximum.Min(x => x.Value);

            // Updatet the chart minimum y with the lowest minimum
            chart.MinY = new List<double> { minMin, minMeas, minMax }.Min();

            // Get the maximum value of each of the three series
            double maxMin = chart.ChartValuesMinimum.Max(x => x.Value);
            double maxMeas = chart.ChartValuesMeasured.Max(x => x.Value);
            double maxMax = chart.ChartValuesMaximum.Max(x => x.Value);

            // Updatet the chart maximum y with the highest maximum
            chart.MaxY = new List<double> { maxMin, maxMeas, maxMax }.Max();

            // Adjust the scales if the minimum = the maximum, adjust them
            if (chart.MaxY == chart.MinY)
            {
                if (chart.MaxY > 0)
                {
                    chart.MaxY *= 1.1;
                    chart.MinY *= 0.9;
                }
                else if (chart.MaxY < 0)
                {
                    chart.MaxY *= -0.9;
                    chart.MinY *= -1.1;
                }
                else
                {
                    chart.MaxY = +1.0;
                    chart.MinY = -1.0;
                }
            }
        }
        #endregion
    }
}
