﻿namespace LoggingAndChartingDemo
{
    using LiveCharts;
    using LiveCharts.Configurations;
    using System.Collections.Generic;
    using System.ComponentModel;
    using System.Windows.Controls;

    /// <summary>
    /// Interaction logic for MinMeasMaxLineChart.xaml
    /// </summary>
    public partial class MinMeasMaxLineChart : UserControl, INotifyPropertyChanged
    {
        #region Name
        private string chartName;
        public string ChartName
        {
            get { return chartName; }
            set
            {
                if (value != chartName)
                {
                    chartName = value;
                    PropertyChanged.Invoke(sender: this, e: new PropertyChangedEventArgs("ChartName"));
                }
            }
        }
        #endregion

        #region Minimum, Measured, and Maximum ChartValues
        public ChartValues<MeasureModel> ChartValuesMinimum { get; set; } = new ChartValues<MeasureModel>(new List<MeasureModel>());
        public ChartValues<MeasureModel> ChartValuesMeasured { get; set; } = new ChartValues<MeasureModel>(new List<MeasureModel>());
        public ChartValues<MeasureModel> ChartValuesMaximum { get; set; } = new ChartValues<MeasureModel>(new List<MeasureModel>());
        #endregion

        #region MinY
        private double minY = -1.0;
        public double MinY
        {
            get { return minY; }
            set
            {
                if (value != minY)
                {
                    minY = value;
                    PropertyChanged.Invoke(sender: this, e: new PropertyChangedEventArgs("MinY"));
                }
            }
        }
        #endregion

        #region MaxY
        private double maxY = +1.0;
        public double MaxY
        {
            get { return maxY; }
            set
            {
                if (value != maxY)
                {
                    maxY = value;
                    PropertyChanged.Invoke(sender: this, e: new PropertyChangedEventArgs("MaxY"));
                }
            }
        } 
        #endregion

        public double SeparatorStepY { get; set; } = 0.1;

        #region Constructor
        public MinMeasMaxLineChart()
        {
            InitializeComponent();

            var mapper = Mappers.Xy<MeasureModel>()
                .X(model => model.Index)
                .Y(model => model.Value);

            Charting.For<MeasureModel>(mapper);

            DataContext = this;
        }
        #endregion

        #region INotifyPropertyChanged Implementation
        public event PropertyChangedEventHandler PropertyChanged = delegate { };
        #endregion
    }
}
