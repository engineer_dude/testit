﻿namespace LoggingAndChartingDemo
{
    public class MeasureModel
    {
        public int Index { get; set; }
        public double Value { get; set; }
    }
}
