﻿namespace TestEquipmentIO_Demo
{
    using TestIt;
    using Agilent_82357B_USB_GPIB_Controller;
    using A34970A;
    using E3631A;
    using Oscilloscope_7000Series;
    using static System.Console;

    /*  Dependencies: 34970A, 34901A, Keysight 82357B, and HP/Agilent/Keysight E3631A power supply
     *  and an Agilent DSO7034B with an address alias set to USBInstrument1
     *  The output of the power supply (P6V) needs to be connected to CH1 of a 34901A module in slot 2 of the 34970A
    */

    class TestEquipmentIO_Demo
    {
        static void Main(string[] args)
        {
            ConnectableIO gpibController = new Agilent_82357B_Controller(controllerAddress: "GPIB0");
            E3631A powerSupply = new E3631A(gpibController, "5");
            A34970A multimeter = new A34970A(controller: gpibController, address: "11");

            // TODO: Determine address for oscilloscope
            Oscilloscope_7000Series scope = new Oscilloscope_7000Series(controller: gpibController, address: "USBInstrument1");

            (bool success, string identity, string info) powerSupplyIdResult = powerSupply.Identify();

            WriteLine("------------------------------------------------------------------------------------");
            WriteLine($"The power supply ID success is : '{powerSupplyIdResult.success}'");
            WriteLine($"The power supply ID identity is: '{powerSupplyIdResult.identity}'");
            WriteLine($"The power supply ID info is    : '{powerSupplyIdResult.info}'");

            (bool success, string identity, string info) multimeterIdResult = multimeter.Identify();

            WriteLine("------------------------------------------------------------------------------------");
            WriteLine($"The multimeter ID success is : '{multimeterIdResult.success}'");
            WriteLine($"The multimeter ID identity is: '{multimeterIdResult.identity}'");
            WriteLine($"The multimeter ID info is    : '{multimeterIdResult.info}'");

            (bool success, string identity, string info) scopeIdResult = scope.Identify();

            WriteLine("------------------------------------------------------------------------------------");
            WriteLine($"The scope ID success is : '{scopeIdResult.success}'");
            WriteLine($"The scope ID identity is: '{scopeIdResult.identity}'");
            WriteLine($"The scope ID info is    : '{scopeIdResult.info}'");

            (bool success, string info) setOutputResult = powerSupply.SetOutput(E3631A.OUTPUT_STATE.ON);

            WriteLine("------------------------------------------------------------------------------------");
            WriteLine($"The Set Output success is : '{setOutputResult.success}'");
            WriteLine($"The Set OUtput info is    : '{setOutputResult.info}'");


            (bool success, string info) setVoltageResult = powerSupply.SetVoltage(E3631A.CHANNEL.P6V, 1.23);

            WriteLine("------------------------------------------------------------------------------------");
            WriteLine($"The Set Voltage success is : '{setVoltageResult.success}'");
            WriteLine($"The Set Voltage info is    : '{setVoltageResult.info}'");


            (bool success, double voltage, string info) measureVDCResult = multimeter.MeasureVDC(range: 2.1, resolution: 0.001, channel: 201);

            WriteLine("------------------------------------------------------------------------------------");
            WriteLine($"The Measure Voltage success is : '{measureVDCResult.success}'");
            WriteLine($"The Measure Voltage value is   : '{measureVDCResult.voltage}'");
            WriteLine($"The Measure Voltage info is    : '{measureVDCResult.info}'");

            Write("\n<ENTER> to exit: ");
            ReadLine();
        }
    }
}
