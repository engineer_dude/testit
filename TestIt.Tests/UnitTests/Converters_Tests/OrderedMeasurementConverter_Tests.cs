﻿namespace TestIt.Tests.UnitTests
{
    using System;
    using System.Drawing;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class OrderedMeasurementConverter_Tests
    {
        private OrderedMeasurementConverter<Color> orderedMeasurementConverter;
        private ColorMeasurement colorMeasurement;

        [TestInitialize]
        public void Initialize()
        {
            orderedMeasurementConverter = new OrderedMeasurementConverter<Color>();
            colorMeasurement = new ColorMeasurement();
        }

        [TestMethod]
        public void GivenEmptyConversionDictionaryAndDefaultColorMeasurement_WhenConvert_ThenDefaultInt32Measurement()
        {
            Int32Measurement int32Measurement = orderedMeasurementConverter.Convert(colorMeasurement);

            Assert.AreEqual(expected: true, actual: int32Measurement.IsNumeric);
            Assert.AreEqual(expected: 0, actual: int32Measurement.Maximum);
            Assert.AreEqual(expected: 0, actual: int32Measurement.Measured);
            Assert.AreEqual(expected: typeof(Int32), actual: int32Measurement.MeasurementType);
            Assert.AreEqual(expected: 0, actual: int32Measurement.Minimum);
            Assert.AreEqual(expected: "", actual: int32Measurement.Name);
            Assert.AreEqual(expected: true, actual: int32Measurement.Pass);
            Assert.AreEqual(expected: new DateTime(), actual: int32Measurement.Timestamp);
            Assert.AreEqual(expected: "", actual: int32Measurement.Unit);
        }

        [TestMethod]
        public void GivenEmptyConversionDictionaryAndColorMeasurementWithMinimumEqualsBlue_WhenOriginal_ThenGetBlue()
        {
            colorMeasurement.Minimum = Color.Blue;
            Int32Measurement int32Measurement = orderedMeasurementConverter.Convert(colorMeasurement);
            Color originalColor = (Color)orderedMeasurementConverter.Original(0);

            Assert.AreEqual(expected: Color.Blue, actual: originalColor);
        }
    }
}
