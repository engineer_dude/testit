﻿namespace TestIt.Tests.UnitTests
{
    using System.Drawing;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class OrderedMeasurementConverterFactory_Tests
    {
        [TestMethod]
        public void GivenMeasurementTypeIsColorMeasurement_WhenConvert_ThenNotNull()
        {
            OrderedMeasurementConverterFactory factory = new OrderedMeasurementConverterFactory();
            MeasurementConverter converter = factory.Create(measurementType: typeof(ColorMeasurement));

            Assert.IsNotNull(converter);
        }

        [TestMethod]
        public void GivenMeasurementTypeIsDoubleMeasurement_WhenConvert_ThenNull()
        {
            OrderedMeasurementConverterFactory factory = new OrderedMeasurementConverterFactory();
            MeasurementConverter converter = factory.Create(measurementType: typeof(DoubleMeasurement));

            Assert.IsNull(converter);
        }
    }
}
