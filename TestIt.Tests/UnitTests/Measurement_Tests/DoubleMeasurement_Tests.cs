﻿namespace TestIt.Tests.UnitTests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;

    [TestClass]
    public class DoubleMeasurement_Tests
    {
        Measurement measurement;

        [TestInitialize]
        public void Initialize()
        {
            measurement = new DoubleMeasurement();
        }

        [TestMethod]
        public void VerifyEmptyDoubleMeasurementDefaultProperties()
        {
            Assert.AreEqual(double.NaN, measurement.Maximum);
            Assert.AreEqual(double.NaN, measurement.Measured);
            Assert.AreEqual(double.NaN, measurement.Minimum);
            Assert.AreEqual("", measurement.Name);
            Assert.AreEqual(true, measurement.Pass);
            Assert.AreEqual(new DateTime(), measurement.Timestamp);
            Assert.AreEqual("", measurement.Unit);
            Assert.IsTrue(measurement.IsNumeric);
            Assert.AreEqual(expected: typeof(double), actual: measurement.MeasurementType);
        }

        [TestMethod]
        public void GivenDefaultDoubleMeasurement_WhenToCSV_ExpectedDefaultValues()
        {
            string value = measurement.ToCSV();

            string expected = "";
            expected += "TestIt.DoubleMeasurement,";
            expected += "1/1/0001 12:00:00 AM,";
            expected += ",";
            expected += "NaN,";
            expected += "NaN,";
            expected += "NaN,";
            expected += ",";
            expected += "True";

            Assert.AreEqual(expected: expected, actual: value);
        }

        [TestMethod]
        public void GivenDefaultDoubleMeasurement_WhenFromCSV_ExpectedDefaultValues()
        {
            string csv = "";
            csv += "TestIt.DoubleMeasurement,";
            csv += "1/1/0001 12:00:00 AM,";
            csv += ",";
            csv += "NaN,";
            csv += "NaN,";
            csv += "NaN,";
            csv += ",";
            csv += "True";

            measurement = measurement.FromCSV(csv) as DoubleMeasurement;

            Assert.AreEqual(double.NaN, measurement.Maximum);
            Assert.AreEqual(double.NaN, measurement.Measured);
            Assert.AreEqual(double.NaN, measurement.Minimum);
            Assert.AreEqual("", measurement.Name);
            Assert.AreEqual(true, measurement.Pass);
            Assert.AreEqual(new DateTime(), measurement.Timestamp);
            Assert.AreEqual("", measurement.Unit);
            Assert.IsTrue(measurement.IsNumeric);
            Assert.AreEqual(expected: typeof(double), actual: measurement.MeasurementType);
        }
    }
}
