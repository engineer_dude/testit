﻿namespace TestIt.Tests.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Drawing;

    [TestClass]
    public class ColorMeasurement_Tests
    {
        Measurement measurement;
        [TestInitialize]
        public void Initialize()
        {
            measurement = new ColorMeasurement();
        }

        [TestMethod]
        public void VerifyEmptyColorMeasurementDefaultProperties()
        {
            Assert.AreEqual(Color.Empty, measurement.Maximum);
            Assert.AreEqual(Color.Empty, measurement.Measured);
            Assert.AreEqual(Color.Empty, measurement.Minimum);
            Assert.AreEqual("", measurement.Name);
            Assert.AreEqual(true, measurement.Pass);
            Assert.AreEqual(new DateTime(), measurement.Timestamp);
            Assert.AreEqual("", measurement.Unit);
            Assert.IsFalse(measurement.IsNumeric);
            Assert.AreEqual(expected: typeof(Color), actual: measurement.MeasurementType);
            Assert.AreEqual(expected: 0, actual: ((ColorMeasurement)measurement).ConversionDictionary.Count);
        }

        [TestMethod]
        public void GivenDefaultColorMeasurement_WhenToCSV_ExpectedDefaultValues()
        {
            string value = measurement.ToCSV();

            string expected = "";
            expected += "TestIt.ColorMeasurement,";
            expected += "1/1/0001 12:00:00 AM,";
            expected += ",";
            expected += "0,";
            expected += "0,";
            expected += "0,";
            expected += ",";
            expected += "True";

            Assert.AreEqual(expected: expected, actual: value);
        }

        [TestMethod]
        public void GivenDefaultColorMeasurement_WhenFromCSV_ExpectedDefaultValues()
        {
            string csv = "";
            csv += "TestIt.ColorMeasurement,";
            csv += "1/1/0001 12:00:00 AM,";
            csv += ",";
            csv += "0,";
            csv += "0,";
            csv += "0,";
            csv += ",";
            csv += "True";

            measurement = measurement.FromCSV(csv) as ColorMeasurement;

            Assert.AreEqual(Color.FromName(Color.Empty.Name), measurement.Maximum);
            Assert.AreEqual(Color.FromName(Color.Empty.Name), measurement.Measured);
            Assert.AreEqual(Color.FromName(Color.Empty.Name), measurement.Minimum);
            Assert.AreEqual("", measurement.Name);
            Assert.AreEqual(true, measurement.Pass);
            Assert.AreEqual(new DateTime(), measurement.Timestamp);
            Assert.AreEqual("", measurement.Unit);
            Assert.IsFalse(measurement.IsNumeric);
            Assert.AreEqual(expected: typeof(Color), actual: measurement.MeasurementType);
            Assert.AreEqual(expected: 0, actual: ((ColorMeasurement)measurement).ConversionDictionary.Count);
        }
    }
}
