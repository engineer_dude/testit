﻿namespace TestIt.Tests.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;

    [TestClass]
    public class Int32Measurement_Tests
    {
        Measurement measurement;

        [TestInitialize]
        public void Initialize()
        {
            measurement = new Int32Measurement();
        }

        [TestMethod]
        public void VerifyEmptyDoubleMeasurementDefaultProperties()
        {
            Assert.AreEqual(expected: 0, actual: measurement.Maximum);
            Assert.AreEqual(expected: 0, actual: measurement.Measured);
            Assert.AreEqual(expected: typeof(Int32), actual: measurement.MeasurementType);
            Assert.AreEqual(expected: 0, actual: measurement.Minimum);
            Assert.AreEqual(expected: "", actual: measurement.Name);
            Assert.AreEqual(expected: true, actual: measurement.Pass);
            Assert.AreEqual(expected: new System.DateTime(), actual: measurement.Timestamp);
            Assert.AreEqual(expected: "", actual: measurement.Unit);
            Assert.IsTrue(measurement.IsNumeric);
            Assert.AreEqual(expected: typeof(Int32), actual: measurement.MeasurementType);
        }

        [TestMethod]
        public void GivenDefaultInt32Measurement_WhenToCSV_ExpectedDefaultValues()
        {
            string value = measurement.ToCSV();

            string expected = "";
            expected += "TestIt.Int32Measurement,";
            expected += "1/1/0001 12:00:00 AM,";
            expected += ",";
            expected += "0,";
            expected += "0,";
            expected += "0,";
            expected += ",";
            expected += "True";

            Assert.AreEqual(expected: expected, actual: value);
        }

        [TestMethod]
        public void GivenDefaultInt32Measurement_WhenFromCSV_ExpectedDefaultValues()
        {
            string csv = "";
            csv += "TestIt.Int32Measurement,";
            csv += "1/1/0001 12:00:00 AM,";
            csv += ",";
            csv += "0,";
            csv += "0,";
            csv += "0,";
            csv += ",";
            csv += "True";

            measurement = measurement.FromCSV(csv) as Int32Measurement;

            Assert.AreEqual(0, measurement.Maximum);
            Assert.AreEqual(0, measurement.Measured);
            Assert.AreEqual(0, measurement.Minimum);
            Assert.AreEqual("", measurement.Name);
            Assert.AreEqual(true, measurement.Pass);
            Assert.AreEqual(new DateTime(), measurement.Timestamp);
            Assert.AreEqual("", measurement.Unit);
            Assert.IsTrue(measurement.IsNumeric);
            Assert.AreEqual(expected: typeof(Int32), actual: measurement.MeasurementType);
        }
    }
}
