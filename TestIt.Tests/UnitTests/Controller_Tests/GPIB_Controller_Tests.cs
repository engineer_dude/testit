﻿namespace TestIt.Tests.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using MockGPIB_Controller;

    [TestClass]
    public class GPIB_Controller_Tests
    {
        MockGPIB_Controller controller;

        [TestInitialize]
        public void Initialize()
        {
            controller = new MockGPIB_Controller("GPIB0");
        }

        [TestMethod]
        public void SetAddress()
        {
            controller.Address = "6";

            Assert.AreEqual("6", controller.Address);
        }

        [TestMethod]
        public void GetAddress()
        {
            Assert.AreEqual("GPIB0", controller.Address);
        }

        [TestClass]
        public class GivenControllerIsConnected
        {
            private MockGPIB_Controller controller;
            private readonly string address = "GPIB0";
            private readonly string connectAddress = "GPIB0::1::INSTR";

            [TestInitialize]
            public void Initialize()
            {
                controller = new MockGPIB_Controller(address)
                {
                    Address = address,
                };

                controller.Connect(connectAddress);
            }

            [TestMethod]
            public void WhenConnect_ThenFail()
            {
                (bool success, string info) = controller.Connect(address: connectAddress);

                Assert.AreEqual(expected: false, actual: success, message: info);
                Assert.AreEqual(expected: "Error - Already connected", actual: info);
            }

            [TestMethod]
            public void WhenDisconnect_ThenSuccess()
            {
                (bool success, string info) = controller.Disconnect();

                Assert.IsTrue(success, info);
            }

            [TestMethod]
            public void WhenWrite_ThenSuccess()
            {
                (bool success, string info) = controller.Write("X");

                Assert.IsTrue(success, info);

                Assert.AreEqual(expected: "X", actual: controller.ValueWritten);
            }

            [TestMethod]
            public void WhenWriteAndRead_ThenSuccess()
            {
                (bool success, string info) writeResult = controller.Write("OUT OFF");
                (bool success, string valueRead, string info) readResult = controller.Read();

                bool success = writeResult.success && readResult.success;
                string info = writeResult.info + "\n" + readResult.info;

                Assert.IsTrue(success, info);

                Assert.AreEqual(expected: "OUT OFF", actual: controller.ValueWritten);
            }

            [TestMethod]
            public void WhenRead_ThenSuccess()
            {
                (bool success, string valueRead, string info) = controller.Read();

                Assert.IsTrue(success, info);
                Assert.AreEqual(expected: "", actual: valueRead);
                Assert.AreEqual(expected: "Success", actual: info);
            }
        }

        [TestClass]
        public class GivenControllerNotConnected
        {
            private MockGPIB_Controller controller;
            private readonly string address = "GPIB0";
            private readonly string connectAddress = "GPIB0::1::INSTR";

            [TestInitialize]
            public void Initialize()
            {
                controller = new MockGPIB_Controller(address)
                {
                    Address = address,
                };
            }

            [TestMethod]
            public void WhenConnect_ThenSuccess()
            {
                (bool success, string info) = controller.Connect(address: connectAddress);

                Assert.AreEqual(expected: true, actual: success, message: info);
                Assert.AreEqual(expected: "Successfully connected", actual: info);
            }

            [TestMethod]
            public void WhenDisconnect_ThenSuccess()
            {
                (bool success, string info) = controller.Disconnect();

                Assert.IsTrue(success, info);
            }

            [TestMethod]
            public void WhenWrite_ThenFail()
            {
                (bool success, string info) = controller.Write("X");

                Assert.IsFalse(success, info);

                Assert.AreEqual(expected: "", actual: controller.ValueWritten);
            }

            [TestMethod]
            public void WhenRead_ThenFail()
            {
                (bool success, string valueRead, string info) = controller.Read();

                Assert.IsFalse(success, info);
                Assert.AreEqual(expected: "", actual: valueRead);
                Assert.AreEqual(expected: "Error - Connection is closed", actual: info);
            }
        }
    }
}
