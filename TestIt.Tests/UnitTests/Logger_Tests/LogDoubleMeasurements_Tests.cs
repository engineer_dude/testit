﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;

namespace TestIt.Tests.UnitTests
{
    [TestClass]
    public class LogDoubleMeasurements_Tests
    {
        private Logger logger;

        public string TheProperty { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            logger = new PropertyLogger(instance: this, propertyName: "TheProperty");
        }

        [TestMethod]
        public void LogEmptyDoubleMeasurement()
        {
            Measurement measurement = new DoubleMeasurement();

            logger.LogMeasurement(measurement);

            Assert.AreEqual("TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,NaN,NaN,NaN,,True\r\n", TheProperty);
        }

        [TestMethod]
        public void ReadLoggedEmptyDoubleMeasurement()
        {
            Measurement measurement = new DoubleMeasurement();

            logger.LogMeasurement(measurement);

            Measurement measurementFromCSV = measurement.FromCSV(TheProperty);

            Assert.AreEqual(typeof(double), measurementFromCSV.MeasurementType);
            Assert.AreEqual(new DateTime(1, 1, 1, 00, 00, 00), measurementFromCSV.Timestamp);
            Assert.AreEqual("", measurementFromCSV.Name);
            Assert.AreEqual(double.NaN, measurementFromCSV.Minimum);
            Assert.AreEqual(double.NaN, measurementFromCSV.Measured);
            Assert.AreEqual(double.NaN, measurementFromCSV.Maximum);
            Assert.AreEqual("", measurementFromCSV.Unit);
            Assert.AreEqual(true, measurementFromCSV.Pass);
        }

        [TestMethod]
        public void CreateDoubleMeasurementFromLoggedNonEmptyDoubleMeasurement()
        {
            Measurement measurement = new DoubleMeasurement
            {
                Maximum = 3.0,
                Measured = 2.0,
                Minimum = 1.0,
                Name = "VIN",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "V"
            };

            logger.LogMeasurement(measurement);

            Measurement measurementFromCSV = measurement.FromCSV(TheProperty);

            Assert.AreEqual(typeof(double), measurementFromCSV.MeasurementType);
            Assert.AreEqual(new DateTime(2019, 1, 18, 15, 07, 00), measurementFromCSV.Timestamp);
            Assert.AreEqual("VIN", measurementFromCSV.Name);
            Assert.AreEqual(1.0, measurementFromCSV.Minimum);
            Assert.AreEqual(2.0, measurementFromCSV.Measured);
            Assert.AreEqual(3.0, measurementFromCSV.Maximum);
            Assert.AreEqual("V", measurementFromCSV.Unit);
            Assert.AreEqual(true, measurementFromCSV.Pass);
        }

        [TestMethod]
        public void LogTwoDoubleMeasurementsAndRetrieveTwoDoubleMeasurements()
        {
            Measurement meas1 = new DoubleMeasurement
            {
                Maximum = 3.0,
                Measured = 2.0,
                Minimum = 1.0,
                Name = "VIN",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "V"
            };

            logger.LogMeasurement(meas1);

            Measurement meas2 = new DoubleMeasurement
            {
                Maximum = 3.0,
                Measured = 2.2,
                Minimum = 1.0,
                Name = "VIN",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "V"
            };

            logger.LogMeasurement(meas2);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            Assert.AreEqual(new DateTime(2019, 1, 18, 15, 07, 00), measurements[0].Timestamp);
            Assert.AreEqual("VIN", measurements[0].Name);
            Assert.AreEqual(1.0, measurements[0].Minimum);
            Assert.AreEqual(2.0, measurements[0].Measured);
            Assert.AreEqual(3.0, measurements[0].Maximum);
            Assert.AreEqual("V", measurements[0].Unit);
            Assert.AreEqual(true, measurements[0].Pass);

            Assert.AreEqual(new DateTime(2019, 1, 18, 15, 07, 00), measurements[1].Timestamp);
            Assert.AreEqual("VIN", measurements[1].Name);
            Assert.AreEqual(1.0, measurements[1].Minimum);
            Assert.AreEqual(2.2, measurements[1].Measured);
            Assert.AreEqual(3.0, measurements[1].Maximum);
            Assert.AreEqual("V", measurements[1].Unit);
            Assert.AreEqual(true, measurements[1].Pass);
        }

        [TestMethod]
        public void GetTwoLoggedDoubleMeasurementsFromCSV_CalculateAverage()
        {
            Measurement meas1 = new DoubleMeasurement
            {
                Maximum = 3.0,
                Measured = 2.0,
                Minimum = 1.0,
                Name = "VIN",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "V"
            };

            logger.LogMeasurement(meas1);

            Measurement meas2 = new DoubleMeasurement
            {
                Maximum = 3.0,
                Measured = 2.2,
                Minimum = 1.0,
                Name = "VIN",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "V"
            };

            logger.LogMeasurement(meas2);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            double sum = 0;

            foreach (Measurement measurement in measurements)
            {
                var a = (double)measurement.Measured;
                sum += a;
            }

            sum /= measurements.Count;

            Assert.AreEqual(2.1, sum);
        }

        #region Invalid number of parts in the line
        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_1_Part_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 1", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_2_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 2", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_3_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 3", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_4_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 4", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_5_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 5", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_6_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 6", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_7_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 7", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_9_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 9", actual: ex.Message);
            }
        }
        #endregion

        #region Parsing
        [TestMethod]
        public void GivenInvalidTimestamp_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,A1/1/0001 12:00:00 AM,,0,0,0,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The string was not recognized as a valid DateTime"));
            }
        }

        [TestMethod]
        public void GivenInvalidMinimum_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,B,0,0,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "Error converting Minimum", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenInvalidMeasured_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,0,B,0,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "Error converting Measured", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenInvalidMaximum_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,0,0,B,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "Error converting Maximum", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenPassOf_Pass_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,Pass\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "String was not recognized as a valid Boolean.", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenPassOf_true_WhenGetMeasurementsFromCSV_ThenNoException()
        {
            string csv = "TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,true\r\n";

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
        }

        [TestMethod]
        public void GivenPassOf_TRUE_WhenGetMeasurementsFromCSV_ThenNoException()
        {
            string csv = "TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,TRUE\r\n";

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
        }

        [TestMethod]
        public void GivenPassOf_True_WhenGetMeasurementsFromCSV_ThenNoException()
        {
            string csv = "TestIt.DoubleMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,True\r\n";

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
        }
        #endregion
    }
}
