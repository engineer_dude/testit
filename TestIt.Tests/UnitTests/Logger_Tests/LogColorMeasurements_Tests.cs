﻿namespace TestIt.Tests.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Linq;
    using System.Collections.Generic;
    using System.Drawing;

    [TestClass]
    public class LogColorMeasurements_Tests
    {
        private Logger logger;

        public string TheProperty { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            logger = new PropertyLogger(instance: this, propertyName: "TheProperty");
        }

        [TestMethod]
        public void LogEmptyColorMeasurement()
        {
            Measurement measurement = new ColorMeasurement();

            logger.LogMeasurement(measurement);

            Assert.AreEqual("TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,True\r\n", TheProperty);
        }

        [TestMethod]
        public void ReadLoggedEmptyColorMeasurement()
        {
            Measurement measurement = new ColorMeasurement();

            logger.LogMeasurement(measurement);

            Measurement measurementFromCSV = measurement.FromCSV(TheProperty);

            Assert.AreEqual(typeof(Color), measurementFromCSV.MeasurementType);
            Assert.AreEqual(new DateTime(1, 1, 1, 00, 00, 00), measurementFromCSV.Timestamp);
            Assert.AreEqual("", measurementFromCSV.Name);
            Assert.AreEqual(Color.Empty.Name, ((Color)(measurementFromCSV.Minimum)).Name);
            Assert.AreEqual(Color.Empty.Name, ((Color)(measurementFromCSV.Measured)).Name);
            Assert.AreEqual(Color.Empty.Name, ((Color)(measurementFromCSV.Maximum)).Name);
            Assert.AreEqual("", measurementFromCSV.Unit);
            Assert.AreEqual(true, measurementFromCSV.Pass);
        }

        [TestMethod]
        public void CreateColorMeasurementFromLoggedNonEmptyColorMeasurement()
        {
            Measurement measurement = new ColorMeasurement
            {
                Maximum = Color.Black,
                Measured = Color.Black,
                Minimum = Color.Black,
                Name = "VIN LED",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "State"
            };

            logger.LogMeasurement(measurement);

            Measurement measurementFromCSV = measurement.FromCSV(TheProperty);

            Assert.AreEqual(typeof(Color), measurementFromCSV.MeasurementType);
            Assert.AreEqual(new DateTime(2019, 1, 18, 15, 07, 00), measurementFromCSV.Timestamp);
            Assert.AreEqual("VIN LED", measurementFromCSV.Name);
            Assert.AreEqual(Color.Black, measurementFromCSV.Minimum);
            Assert.AreEqual(Color.Black, measurementFromCSV.Measured);
            Assert.AreEqual(Color.Black, measurementFromCSV.Maximum);
            Assert.AreEqual("State", measurementFromCSV.Unit);
            Assert.AreEqual(true, measurementFromCSV.Pass);
        }

        [TestMethod]
        public void LogTwoColorMeasurementsAndRetrieveTwoColorMeasurements()
        {
            Measurement meas1 = new ColorMeasurement
            {
                Maximum = Color.Black,
                Measured = Color.Blue,
                Minimum = Color.Black,
                Name = "VIN LED",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "State"
            };

            logger.LogMeasurement(meas1);

            Measurement meas2 = new ColorMeasurement
            {
                Maximum = Color.Blue,
                Measured = Color.Blue,
                Minimum = Color.Blue,
                Name = "VOUT LED",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "State"
            };

            logger.LogMeasurement(meas2);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            Assert.AreEqual(new DateTime(2019, 1, 18, 15, 07, 00), measurements[0].Timestamp);
            Assert.AreEqual("VIN LED", measurements[0].Name);
            Assert.AreEqual(Color.Black, measurements[0].Minimum);
            Assert.AreEqual(Color.Blue, measurements[0].Measured);
            Assert.AreEqual(Color.Black, measurements[0].Maximum);
            Assert.AreEqual("State", measurements[0].Unit);
            Assert.AreEqual(false, measurements[0].Pass);

            Assert.AreEqual(new DateTime(2019, 1, 18, 15, 07, 00), measurements[1].Timestamp);
            Assert.AreEqual("VOUT LED", measurements[1].Name);
            Assert.AreEqual(Color.Blue, measurements[1].Minimum);
            Assert.AreEqual(Color.Blue, measurements[1].Measured);
            Assert.AreEqual(Color.Blue, measurements[1].Maximum);
            Assert.AreEqual("State", measurements[1].Unit);
            Assert.AreEqual(true, measurements[1].Pass);
        }

        [TestMethod]
        public void GetTwoLoggedColorMeasurementsFromCSV_GetUniqueValues()
        {
            Measurement meas1 = new ColorMeasurement
            {
                Maximum = Color.Black,
                Measured = Color.Blue,
                Minimum = Color.Black,
                Name = "VIN LED",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "State"
            };

            logger.LogMeasurement(meas1);

            Measurement meas2 = new ColorMeasurement
            {
                Maximum = Color.Red,
                Measured = Color.Green,
                Minimum = Color.Red,
                Name = "VOUT LED",
                Timestamp = new DateTime(year: 2019, month: 1, day: 18, hour: 15, minute: 7, second: 0),
                Unit = "State"
            };

            logger.LogMeasurement(meas2);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            List<Color> actualColors = new List<Color>();

            foreach (Measurement measurement in measurements)
            {
                Color color = (Color)measurement.Minimum;
                if (actualColors.Contains(color) == false)
                {
                    actualColors.Add((Color)measurement.Minimum);
                }

                color = (Color)measurement.Measured;
                if (actualColors.Contains(color) == false)
                {
                    actualColors.Add((Color)measurement.Measured);
                }

                color = (Color)measurement.Maximum;
                if (actualColors.Contains(color) == false)
                {
                    actualColors.Add((Color)measurement.Maximum);
                }
            }

            List<Color> expectedColors = new List<Color>
            {
                Color.Black,
                Color.Blue,
                Color.Red,
                Color.Green
            };

            Assert.IsTrue(expectedColors != null && actualColors != null & expectedColors.Count == actualColors.Count);

            for (int i = 0; i < expectedColors.Count; i++)
            {
                Assert.AreEqual(expectedColors[i], actualColors[i]);
            }
        }

        [TestMethod]
        public void GivenZeroCommaSeparatedParts_ThenArgumentException()
        {
            string csv = "";

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);

            Assert.AreEqual(expected: 0, actual: measurements.Count);
        }

        [TestMethod]
        public void GivenInvalidMeasuremenType_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "A" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "The factory does not create the type 'A'", actual: ex.Message);
            }
        }

        #region Invalid number of parts in the line
        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_1_Part_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 1", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_2_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 2", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_3_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 3", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_4_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 4", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_5_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 5", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_6_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 6", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_7_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 7", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenValidMeasuremenTypeAnd_9_Parts_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += ",";
            csv += "" + Environment.NewLine;

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: $"Expected 8 parts, but found 9", actual: ex.Message);
            }
        }
        #endregion

        #region Parsing
        [TestMethod]
        public void GivenInvalidTimestamp_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,A1/1/0001 12:00:00 AM,,0,0,0,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.IsTrue(ex.Message.StartsWith("The string was not recognized as a valid DateTime"));
            }
        }

        [TestMethod]
        public void GivenInvalidMinimum_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,B,0,0,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "Error converting Minimum", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenInvalidMeasured_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,0,B,0,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "Error converting Measured", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenInvalidMaximum_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,0,0,B,,True\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "Error converting Maximum", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenPassOf_Pass_WhenGetMeasurementsFromCSV_ThenArgumentException()
        {
            string csv = "TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,Pass\r\n";

            try
            {
                List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
                Assert.Fail("ArgumentException expected");
            }
            catch (ArgumentException ex)
            {
                Assert.AreEqual(expected: "String was not recognized as a valid Boolean.", actual: ex.Message);
            }
        }

        [TestMethod]
        public void GivenPassOf_true_WhenGetMeasurementsFromCSV_ThenNoException()
        {
            string csv = "TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,true\r\n";

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
        }

        [TestMethod]
        public void GivenPassOf_TRUE_WhenGetMeasurementsFromCSV_ThenNoException()
        {
            string csv = "TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,TRUE\r\n";

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
        }

        [TestMethod]
        public void GivenPassOf_True_WhenGetMeasurementsFromCSV_ThenNoException()
        {
            string csv = "TestIt.ColorMeasurement,1/1/0001 12:00:00 AM,,0,0,0,,True\r\n";

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(csv);
        } 
        #endregion
    }
}
