﻿namespace TestIt.Tests.UnitTests
{
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System;
    using System.Collections.Generic;
    using System.Drawing;

    [TestClass]
    public class PropertyLogger_Tests
    {
        private Logger logger;
        public string TheProperty { get; set; }

        [TestInitialize]
        public void Initialize()
        {
            logger = new PropertyLogger(instance: this, propertyName: "TheProperty");
        }

        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteDoubleMeasurement_ExpectOneMeasurementWritten()
        {
            Measurement measurement = new DoubleMeasurement();

            logger.LogMeasurement(measurement);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            Assert.AreEqual(expected: 1, actual: measurements.Count);

            Assert.AreEqual(expected: true, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Maximum);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(double), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);
        }

        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteInt32Measurement_ExpectOneMeasurementWritten()
        {
            Measurement measurement = new Int32Measurement();

            logger.LogMeasurement(measurement);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            Assert.AreEqual(expected: 1, actual: measurements.Count);

            Assert.AreEqual(expected: true, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: 0, actual: measurements[0].Maximum);
            Assert.AreEqual(expected: 0, actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(Int32), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: 0, actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);
        }

        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteColorMeasurement_ExpectOneMeasurementWritten()
        {
            Measurement measurement = new ColorMeasurement();

            logger.LogMeasurement(measurement);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            Assert.AreEqual(expected: 1, actual: measurements.Count);

            Assert.AreEqual(expected: false, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: Color.FromName(Color.Empty.Name), actual: measurements[0].Maximum);
            Assert.AreEqual(expected: Color.FromName(Color.Empty.Name), actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(Color), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: Color.FromName(Color.Empty.Name), actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);
        }

        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteTwoMeasurement_ExpectTwoMeasurementWritten()
        {
            Measurement measurement = new DoubleMeasurement();

            logger.LogMeasurement(measurement);
            logger.LogMeasurement(measurement);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(TheProperty);

            Assert.AreEqual(expected: 2, actual: measurements.Count);

            Assert.AreEqual(expected: true, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Maximum);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(double), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);

            Assert.AreEqual(expected: true, actual: measurements[1].IsNumeric);
            Assert.AreEqual(expected: double.NaN, actual: measurements[1].Maximum);
            Assert.AreEqual(expected: double.NaN, actual: measurements[1].Measured);
            Assert.AreEqual(expected: typeof(double), actual: measurements[1].MeasurementType);
            Assert.AreEqual(expected: double.NaN, actual: measurements[1].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[1].Name);
            Assert.AreEqual(expected: true, actual: measurements[1].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[1].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[1].Unit);
        }
    }
}
