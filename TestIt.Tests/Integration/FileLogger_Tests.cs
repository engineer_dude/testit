﻿namespace TestIt.Tests.Integration
{
    using System;
    using System.IO;
    using System.Collections.Generic;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Drawing;

    [TestClass]
    public class FileLogger_Tests
    {
        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteDoubleMeasurement_ExpectOneMeasurementWritten()
        {
            string filePath = @".\LogFile.csv";

            File.Delete(filePath);

            Logger logger = new FileLogger(filePath);

            Measurement measurement = new DoubleMeasurement();

            logger.LogMeasurement(measurement);

            string content = File.ReadAllText(filePath);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(content);

            Assert.AreEqual(expected: 1, actual: measurements.Count);

            Assert.AreEqual(expected: true, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Maximum);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(double), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);
        }

        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteInt32Measurement_ExpectOneMeasurementWritten()
        {
            string filePath = @".\LogFile.csv";

            File.Delete(filePath);

            Logger logger = new FileLogger(filePath);

            Measurement measurement = new Int32Measurement();

            logger.LogMeasurement(measurement);

            string content = File.ReadAllText(filePath);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(content);

            Assert.AreEqual(expected: 1, actual: measurements.Count);

            Assert.AreEqual(expected: true, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: 0, actual: measurements[0].Maximum);
            Assert.AreEqual(expected: 0, actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(Int32), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: 0, actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);
        }

        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteColorMeasurement_ExpectOneMeasurementWritten()
        {
            string filePath = @".\LogFile.csv";

            File.Delete(filePath);

            Logger logger = new FileLogger(filePath);

            Measurement measurement = new ColorMeasurement();

            logger.LogMeasurement(measurement);

            string content = File.ReadAllText(filePath);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(content);

            Assert.AreEqual(expected: 1, actual: measurements.Count);

            Assert.AreEqual(expected: false, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: Color.FromName(Color.Empty.Name), actual: measurements[0].Maximum);
            Assert.AreEqual(expected: Color.FromName(Color.Empty.Name), actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(Color), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: Color.FromName(Color.Empty.Name), actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);
        }

        [TestMethod]
        [ExpectedException(exceptionType: typeof(ArgumentException))]
        public void GivenInvalidFilePath_WhenWriteMeasurement_ExpectArgumentExceptioon()
        {
            Logger logger = new FileLogger("");

            Measurement measurement = new DoubleMeasurement();

            logger.LogMeasurement(measurement);
        }

        [TestMethod]
        public void GivenEmptyLogFile_WhenWriteTwoMeasurement_ExpectTwoMeasurementWritten()
        {
            string filePath = @".\LogFile.csv";

            File.Delete(filePath);

            Logger logger = new FileLogger(filePath);

            Measurement measurement = new DoubleMeasurement();

            logger.LogMeasurement(measurement);
            logger.LogMeasurement(measurement);

            string content = File.ReadAllText(filePath);

            List<Measurement> measurements = Measurement.GetMeasurementsFromCSV(content);

            Assert.AreEqual(expected: 2, actual: measurements.Count);

            Assert.AreEqual(expected: true, actual: measurements[0].IsNumeric);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Maximum);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Measured);
            Assert.AreEqual(expected: typeof(double), actual: measurements[0].MeasurementType);
            Assert.AreEqual(expected: double.NaN, actual: measurements[0].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[0].Name);
            Assert.AreEqual(expected: true, actual: measurements[0].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[0].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[0].Unit);

            Assert.AreEqual(expected: true, actual: measurements[1].IsNumeric);
            Assert.AreEqual(expected: double.NaN, actual: measurements[1].Maximum);
            Assert.AreEqual(expected: double.NaN, actual: measurements[1].Measured);
            Assert.AreEqual(expected: typeof(double), actual: measurements[1].MeasurementType);
            Assert.AreEqual(expected: double.NaN, actual: measurements[1].Minimum);
            Assert.AreEqual(expected: "", actual: measurements[1].Name);
            Assert.AreEqual(expected: true, actual: measurements[1].Pass);
            Assert.AreEqual(expected: new DateTime(), actual: measurements[1].Timestamp);
            Assert.AreEqual(expected: "", actual: measurements[1].Unit);
        }
    }
}
