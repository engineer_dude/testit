﻿namespace ConsoleApp1
{
    using static System.Console;

    using TestIt;
    using MockGPIB_Controller;
    using Agilent_82357B_USB_GPIB_Controller;
    using SorensenXHR;

    class Program
    {
        static void Main(string[] args)
        {
            ConnectableIO gpibController = new Agilent_82357B_Controller(controllerAddress: "GPIB0");
            SorensenXHR powerSupply = new SorensenXHR(gpibController, address: "1");

            (bool success, string identity, string info) = powerSupply.Identify();

            WriteLine($"The success is :  '{success}'");
            WriteLine($"The identity is: '{identity}'");
            WriteLine($"Info is        : {info}");

            Write("\n<ENTER> to exit: ");
            ReadLine();
        }
    }
}
